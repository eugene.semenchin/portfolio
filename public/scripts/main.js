// Слайдер опыт и верстка проектов

$('.one-time').slick({
  dots: false,
  infinite: true,
  speed: 300,
  autoplay: {
    delay: 2000,
  },
  slidesToShow: 1,
  // adaptiveHeight: true,
  autoplay: true,
  arrows: false
});



// Слайдер отзывы клиентов

$('.multiple-items').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  prevArrow: '<img class="arrow-hover" src="images/prev.svg">',
  nextArrow: '<img class="arrow-hover" src="images/next.svg">',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
  ]
});

// Конверт //

$('.envelope').on('click', function(e) {
  $('.envelope__top').toggleClass('envelope__top_close');
  $('.paper').toggleClass('paper_close');
})




